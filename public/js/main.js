const chatForm= document.getElementById('chat-form');
const chatMessages=document.querySelector('.chat-messages');
const roomName=document.getElementById('room-name');
const userList = document.getElementById('users');

//Obtener el nombre de usuario y su sala por el URL
const {username, room}= Qs.parse(location.search,{
    ignoreQueryPrefix: true
});

console.log(username,room);

const socket = io();

//Entrar a una sala de chat
socket.emit('joinRoom',{username,room});

//Obtener una sala y usuario
socket.on('roomUsers',({room,users})=>{
    outputRoomName(room);
    outputUsers(users);
});

//Mensajes del servidor
socket.on('message', message =>{
    console.log(message);
    outputMessage(message);

    //Desplazamiento hacia abajo cada que se envia un mensaje
    chatMessages.scrollTop = chatMessages.scrollHeight;
});

//Mensaje enviado
chatForm.addEventListener('submit',(e)=>{
    e.preventDefault();
    
    //Obteniendo el mensaje 
    const msg = e.target.elements.msg.value;

    //Emitiendo mensaje al servidor
    socket.emit('chatMessage',msg);

    //limpiar el input
    e.target.elements.msg.value='';
    e.target.elements.msg.focus();

});

//Mensajes de salida para el DOM
function outputMessage(message){
    const div = document.createElement('div');
    div.classList.add('message');
    div.innerHTML=`
    <p class="meta">${message.username}<span>${message.time}</span></p>
	<p class="text">${message.text}</p>
    `;
    document.querySelector('.chat-messages').appendChild(div);
}

//Añadir el nombre de la sala al DOM
function outputRoomName(room){
  roomName.innerText = room;
}

//Añadir usuarios al DOM
function outputUsers(users){
    userList.innerHTML=`
    ${users.map(user => `<li>${user.username}</li>`).join('')}
    `;
}