const path =require('path');
const http=require('http');
const express=require('express');
const socketio=require('socket.io');
const formatMessage=require('./utils/messages')
const  {userJoin,getCurrentUser,getRoomUser,userLeave}  =require('./utils/user')

const app=express();
const server=http.createServer(app);
const io=socketio(server);

app.use(express.static(path.join(__dirname,'public')));
botName='Admin';

io.on('connection',socket =>{
    socket.on('joinRoom',({username,room}) => {
       const user=userJoin(socket.id,username,room);
        socket.join(user.room)

        socket.emit('message',formatMessage(botName,'Bienvenido!'))

    //mandar mensaje cuando un usuario se conecte
    socket.broadcast.to(user.room).emit('message',formatMessage(botName,`${user.username} se ha unido al chat`))
    io.to(user.room).emit('roomUsers',{
        room:user.room,
        users:getRoomUser(user.room)
    })
     
    });
    

    //recibir mensaje y transmitirlo
    socket.on('chatMessage',(msg)=>{
        const user=getCurrentUser(socket.id);
        io.to(user.room).emit('message',formatMessage(user.username,msg));
    })


     //mandar mensaje cuando un usuario de clientes
     socket.on('disconnect',()=>{
         const user=userLeave(socket.id);
         if(user){
        io.to(user.room).emit('message',formatMessage(botName,`${user.username} ha abandonado el chat`))
        io.to(user.room).emit('roomUsers',{
            room:user.room,
            users:getRoomUser(user.room)
        })
    }
    })
})

const port=3000||process.env.PORT;

server.listen(port,()=> console.log(`Servidor corriendo en ${port}`))

